import { Component, OnInit } from '@angular/core';
import { RegistrationUser } from './shared/registration-user';
import { UserRegistrationService } from './services/user-registration.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  //model = new RegistrationUser('','','','','','',null,'SG','','');
  model = null;
  gender: string[] = ['M', 'F'];
  nationalities = [ {desc:'Singapore', value:'SG'}, 
                    {desc: 'Malaysia', value:'MY'}, 
                    {desc:'Thailand', value:'TH'},  
                    {desc:'Vietnam', value: 'VN'}];
  isSubmitted: boolean = false;
  result: string = "";
  toAmend:boolean = false;

  reviews: any;
  users: any;
  above18: boolean =false;

  allEntriesValid:boolean = false;

  genderFieldStatus:boolean =false;

  constructor(private userService: UserRegistrationService){
     // this.above18=false;
      //console.log("above18? " + this.above18);
      }       

  ngOnInit(){

    this.model = new RegistrationUser('','','','','','',null,'','SG','',0);

  }

  // this is to handle when I click on a submit button.
  onSubmit(){
    console.log(this.model.email);
    console.log(this.model.password);
    console.log(this.model.confirmPassword);
    console.log(this.model.firstName);
    console.log(this.model.lastName);
    console.log(this.model.gender);
    console.log(this.model.dateOfBirth);
    console.log(this.model.address);
    console.log(this.model.nationality);
    console.log(this.model.contactNumber);

    this.checkAge();  //To check if the age is 18 or above. 
                      //If not, user have to amend.

    this.checkGender();

    //this.result = JSON.stringify(this.model);
    this.isSubmitted = true;
    this.toAmend = false;
    
  }

    
  amendForm(){
    //console.log("Going To amend Form");
    //window.history.back();
    this.toAmend = true;
    this.isSubmitted = false;
  }
  

  
  onChange(event){

  }

  
  checkAge(){
    let enteredDate = this.model.dateOfBirth;
    console.log("Date of birth: " + enteredDate);
    let y= enteredDate.split("-");

    /**
    //------Just for verifying if the codes are correct-----
    console.log("Entered Year: " +y[0]);
    this.model.dateOfBirth=y[0] + "-" + y[1] + "-" + y[2];
    console.log("New DOB:" + this.model.dateOfBirth);
    //-------------------------------------------------
     */

    let currentTime = new Date();
    let currentYear=currentTime.getFullYear();
    console.log("Current year = " + currentYear);

   // let ageDiff =  currentYear-y[0];
    
    this.model.age=currentYear-y[0];    //To calculate the age of the user

    if(this.model.age >= 18){

      this.above18=true;

      this.allEntriesValid=true;

      this.userService.saveUserRegistration(this.model)
      .subscribe(users => {             
        console.log('send to backend !');
        console.log(users);
        //console.log("Printing \"users\" from \"userService.saveUserRegistraion\": " + users);
        this.users=users;
      })
    }
}


checkGender(){
  if(this.model.gender==""){
    this.genderFieldStatus=false;
  }
  else{
    this.genderFieldStatus=true;
  }

}



}
